rule fasttree:
    '''
    Generate a tree from the MSA with fasttree
    '''
    input:
        rules.concatenate_all_alignments.output
    output:
        'results/tree_fasttree.nwk'
    log:
        'logs/tree_fasttree.log'
    conda:
        '../envs/trees.yaml'
    params:
        fasttree_settings = config['fasttree']['settings']
    threads:
        get_threads('fasttree')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('fasttree', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('fasttree', attempt)
    shell:
        'FastTree {params.fasttree_settings} < {input} > {output}'


rule iqtree:
    '''
    Generate a tree from the MSA with iqtree
    '''
    input:
        rules.concatenate_all_alignments.output
    output:
        output_dir = directory('results/iqtree'),
        tree = 'results/tree_iqtree.nwk'
    log:
        'logs/tree_iqtree.log'
    conda:
        '../envs/trees.yaml'
    params:
        iqtree_settings = config['iqtree']['settings'],
        prefix = 'iqtree'
    threads:
        get_threads('iqtree')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('iqtree', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('iqtree', attempt)
    shell:
        'mkdir {output.output_dir};'
        'iqtree '
        '-s {input} '
        '-nt {threads} '
        '-ntmax {threads} '
        '-mem {resources.mem_mb}M '
        '-pre {output.output_dir}/{params.prefix} '
        '{params.iqtree_settings} 2>&1 > {log};'
        'cp {output.output_dir}/{params.prefix}.treefile {output.tree} 2>&1 >> {log}'


def tree_with_species_names_input(wildcards):
    '''
    Generate the final output of the workflow based on config parameter values.
    '''
    output_string = 'results/tree_{method}.nwk'
    if config['phylogeny_software'] not in {'iqtree', 'fasttree'}:
        logging.error(f'Incorrect value <{config["phylogeny_software"]}> for field'
                       '"phylogeny_software" in the config file. '
                       'Expected values are "iqtree" or "fasttree"')
    return output_string.format(method=config['phylogeny_software'])


rule tree_with_species_names:
    '''
    Creates a copy of the final tree with species names instead of taxids for convenience.
    '''
    input:
        tree = tree_with_species_names_input,
        species_names = rules.get_species_names.output
    output:
        'results/tree_species_names.nwk'
    log:
        'logs/tree_with_species_names.log'
    conda:
        '../envs/trees.yaml'
    threads:
        get_threads('tree_with_species_names')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('tree_with_species_names', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('tree_with_species_names', attempt)
    script:
        '../scripts/tree_with_species_names.py'
