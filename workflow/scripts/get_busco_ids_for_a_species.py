import logging
import os
from utils import setup_logging, load_json_to_dict


SUMMARY_TABLE_PATH_PATTERN = os.path.join('{busco_root_dir}', 'results', 'summary.json')
BUSCO_RESULTS_PATH_PATTERN = os.path.join('{busco_root_dir}', 'results', 'busco', '{accession}', '{dataset}', 'full_table.tsv')


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    busco_output_file_path = snakemake.output[0]
    assembly_output_file_path = snakemake.output[1]
    taxid = int(snakemake.wildcards['taxid'])
    busco_root_dir = snakemake.params.busco_root_dir
    busco_dataset = snakemake.params.busco_dataset

    logging.info(f'Finding best assembly for taxid <{taxid}>')

    # Load data from summary JSON file
    summary_table_path = SUMMARY_TABLE_PATH_PATTERN.format(busco_root_dir=busco_root_dir)
    summary_table = load_json_to_dict(summary_table_path)
    # Find assembly with the highest Complete BUSCO score for the focal taxid
    best_assembly = {'accession': None, 'complete_busco': -1}
    for assembly, data in summary_table.items():
        if data['taxId'] == taxid:
            try:
                busco_data = data['busco'][busco_dataset]
                if float(busco_data['complete']) > best_assembly['complete_busco']:
                    best_assembly['accession'] = assembly
                    best_assembly['complete_busco'] = float(data['busco'][busco_dataset]['complete'])
            except KeyError:
                logging.warning(f'No available BUSCO results using dataset <{busco_dataset}> for assembly {assembly} (taxid: {taxid})')

    if best_assembly['accession'] is None:
        raise KeyError(f'There are no assembly in the current A3Cat release for taxid {taxid}.')

    # Output accession number for the best assembly to the assembly output file
    with open(assembly_output_file_path, 'w') as assembly_output_file:
        assembly_output_file.write(f'{best_assembly["accession"]}\t{best_assembly["complete_busco"]}\n')

    logging.info(f'Best assembly for taxid <{taxid}>: <{best_assembly["accession"]}> (<{best_assembly["complete_busco"]}>% complete BUSCOs)')
    logging.info(f'Retrieving list of single copy complete BUSCOs for <{best_assembly["accession"]}>')

    busco_results_path = BUSCO_RESULTS_PATH_PATTERN.format(busco_root_dir=busco_root_dir,
                                                           accession=best_assembly["accession"],
                                                           dataset=busco_dataset)

    # Save the list of BUSCO ids to the BUSCO output file
    with open(busco_results_path) as busco_results, open(busco_output_file_path, 'w') as output_file:
        busco_count = 0
        for line in busco_results:
            if line[0] == '#':
                continue
            fields = line.split('\t')
            busco_id, status = fields[0], fields[1]
            if status == 'Complete':
                output_file.write(f'{busco_id}\n')
                busco_count += 1

    logging.info(f'Successfully retrieved {busco_count} single copy complete BUSCOs for <{best_assembly["accession"]}>')
