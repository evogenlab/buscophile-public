# Constrain BUSCO ids to not start with a '.' to filter out hidden files
wildcard_constraints:
    busco_id = '[^.].+'

rule align_busco_id:
    '''
    Align sequences for a single BUSCO with muscle
    '''
    input:
        'results/busco_sequences/{busco_id}.faa'
    output:
        'results/alignments/{busco_id}.aln'
    log:
        'logs/alignments/align_{busco_id}.log'
    conda:
        '../envs/alignments.yaml'
    threads:
        get_threads('align_busco_id')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('align_busco_id', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('align_busco_id', attempt)
    shell:
        'muscle -in {input} -out {output} -seqtype protein -quiet'


rule trim_alignment:
    '''
    Trim muscle alignment for a single BUSCO with trimal
    '''
    input:
        'results/alignments/{busco_id}.aln'
    output:
        'results/alignments/{busco_id}.aln.trm'
    log:
        'logs/trim_alignment/trim_{busco_id}.log'
    conda:
        '../envs/alignments.yaml'
    threads:
        get_threads('trim_alignment')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('trim_alignment', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('trim_alignment', attempt)
    shell:
        'trimal -in {input} -out {output} -strictplus'


def get_busco_ids(wildcards):
    '''
    '''
    busco_ids_dir = checkpoints.get_common_busco_ids.get(**wildcards).output[0]
    busco_ids = expand(rules.trim_alignment.output,
                       busco_id=glob_wildcards(os.path.join(busco_ids_dir, '{busco_id}.txt')).busco_id)
    return busco_ids


rule concatenate_all_alignments:
    '''
    Concatenate alignments for all BUSCOs into a single MSA.
    The resulting MSA is complete for all species; when a BUSCO was not found in a species,
    it is inserted as a sequence of '-' with length equal to the BUSCO sequence found in
    the previous species.
    '''
    input:
        get_busco_ids
    output:
        'results/multiple_sequence_alignment.faa'
    log:
        'logs/concatenate_all_alignments.txt'
    conda:
        '../envs/alignments.yaml'
    params:
        taxids = config['taxids']
    threads:
        get_threads('concatenate_all_alignments')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('concatenate_all_alignments', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('concatenate_all_alignments', attempt)
    script:
        '../scripts/concatenate_all_alignments.py'
