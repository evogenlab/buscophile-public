import logging
import os
from utils import setup_logging, load_json_to_dict

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    summary_file_path = snakemake.input.a3cat
    taxid_list = snakemake.input.taxid_list
    species_names_file_path = snakemake.output[0]

    # Load taxids from the user input file
    taxids = set(int(line.rstrip()) for line in open(taxid_list) if len(line) > 2)

    # Load A3Cat data from the summary file
    a3cat_summary = load_json_to_dict(summary_file_path)

    # Retrieve species names from a3cat data
    species_names = {}
    for assembly, data in a3cat_summary.items():
        if data['taxId']in taxids:
            print(data)
            species_names[data['taxId']] = data['taxonomy']['species']

    with open(species_names_file_path, 'w') as species_names_file:
        for taxid, species in species_names.items():
            species_names_file.write(f'{taxid}\t{species}\n')

