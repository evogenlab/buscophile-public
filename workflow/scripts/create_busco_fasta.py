import logging
import os
import tarfile
from utils import setup_logging


BUSCO_RESULTS_PATH_PATTERN = os.path.join('{busco_root_dir}', 'results', 'busco', '{accession}', '{dataset}',
                                          '{accession}_{dataset}.tar.gz')

BUSCO_SEQUENCE_FILE_PATTERN = os.path.join('{accession}', '{dataset}', 'run_{dataset}_odb10', 'busco_sequences',
                                           'single_copy_busco_sequences', '{busco_id}.faa')

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    busco_id = snakemake.wildcards.busco_id
    assembly_file_paths = snakemake.input
    output_file_path = snakemake.output[0]
    busco_root_dir = snakemake.params.busco_root_dir
    busco_dataset = snakemake.params.busco_dataset

    for taxid, assembly_file_path in assembly_file_paths.items():
        # Input associated with the key 'common_buscos' is the directory of common BUSCOS
        # This was needed to execute rules in the correct order; this input is ignored here
        if taxid == 'common_buscos':
            continue
        logging.info(f'Retrieving assembly accession for <{taxid}>')
        with open(assembly_file_path) as assembly_file:
            accession = assembly_file.readline().split('\t')[0]
        busco_results_path = BUSCO_RESULTS_PATH_PATTERN.format(busco_root_dir=busco_root_dir,
                                                               accession=accession,
                                                               dataset=busco_dataset)
        logging.info(f'Retrieving protein sequence for BUSCO id <{busco_id}> in assembly <{accession}> for taxid <{taxid}>')
        # Directly open the relevant file from the tarball
        with tarfile.open(busco_results_path) as busco_results:
            busco_sequence_file_path = BUSCO_SEQUENCE_FILE_PATTERN.format(accession=accession,
                                                                          dataset=busco_dataset,
                                                                          busco_id=busco_id)
            try:
                busco_file = busco_results.extractfile(busco_sequence_file_path)
                lines = busco_file.read().decode('utf-8').split('\n')
                lines[0] = f'>{taxid} {busco_id}'  # Format fasta headers as >taxid busco_id
                with open(output_file_path, 'a') as output_file:
                    output_file.write('\n'.join(lines))
                logging.info(f'Successfully retrieved protein sequence for BUSCO id <{busco_id}> in assembly <{accession}> for taxid <{taxid}>')
            except KeyError:
                logging.info(f'BUSCO <{busco_id}> was not found in assembly <{accession}> for taxid <{taxid}>, skipping')
