# Buscophile

Workflow modified from [Orthophile](https://gitlab.com/evogenlab/orthophile) to generate a simple phylogenetic tree for a set of species from BUSCO results generated with A3Cat.

## Setup

Clone the repository from gitlab:

```bash
# Using https (default way, no setup required)
https://gitlab.com/evogenlab/buscophile.git
```

```bash
# Using ssh (recommended way, requires prior setup)
git@gitlab.com:evogenlab/buscophile.git
```

If you do not have Snakemake and Conda already setup, please follow [the instructions below](#setting-up-conda-and-snakemake). The workflow was tested with **Snakemake v. 6.3.0**.

## Configuring the workflow

All settings are defined and explained in the YAML file `config/config.yaml`.

The only input required by the workflow is a file containing the list of NCBI Taxid for all species to include in the tree, with one Taxid per line as illustrated in the example below:

```
7165
7167
7168
7173
30065
30066
30068
```

The workflow include a default file `config/species.tsv` for convenience.

## Executing the workflow

Simply execute Snakemake from the main workflow directory (*i.e.* the `buscophile` directory created after cloning the repository):

```bash
# You can use more than one core to parallelize jobs by changing the value of --cores
snakemake --cores 1 --use-conda
```

## Output

The output of the workflow is located in the `results` folder as described below:

```bash
# Results folder:
├── alignments/                      # Alignment results for each BUSCO
├── assemblies/                      # Text files with the accession number of the best assembly for each species
├── busco_ids/                       # Text files with all BUSCOs found in the best assembly for each species
├── busco_sequences/                 # Fasta files with the protein sequence of a BUSCO for all species in which it is present
├── common_busco_ids/                # Text files with the BUSCO ID for all BUSCOs found in enough species
├── <iqtree>/                        # Folder with iqtree output (models, etc..) if using iqtree
├── multiple_sequence_alignment.faa  # Concatenated MSA for all BUSCOs
├── tree_<software>.nwk              # Tree generated from the concatenated MSA (default: tree_iqtree.nwk)
└── tree_species_names.nwk           # Same as tree_<software>.nwk but with species names instead of taxids as nodes
```

## Setting up Conda and Snakemake

### Install Conda

Full instructions available [here](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html).

**Download the installer script and run it:**

```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Follow instructions from the prompt. 

**Restart your shell:**

```bash
source ~/.bashrc
```

**Note:** If you're not using bash as your shell, source the appropriate file (*e.g.* `~/.zshrc` if you're using zsh)

**Initialize conda for your shell:**

```bash
# Replace bash with whatever shell you are using
conda init bash
```

**Update conda:**

The Conda version from the official installer is not always the latest update. To make sure Conda is up-to-date, run:

```bash
conda update conda
```

### Install Snakemake

The recommended way to install and run Snakemake is to create a conda environment specifically for it:

```bash
# Create a new empty environment called "snakemake"
conda create --name snakemake
# Activate the environment "snakemake"
conda activate snakemake
# Install snakemake from the Bioconda channel (conda-forge contains dependencies)
conda install -c conda-forge -c bioconda snakemake
```

You can now activate the environment snakemake and run snakemake from it. It is advised to keep the environment as clean as possible, *i.e.* only install software related to running snakemake.

You may need to install [mamba]() (an efficient implementation of conda) because Snakemake uses mamba by default since version 6.1.0. To do so, run 

```bash
conda install -n base -c conda-forge mamba
```
