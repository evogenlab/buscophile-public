'''
Utilities for resources allocation.
'''

import collections
import yaml


def allocate_resources():
    '''
    Get resources requirements for all rules from the resources defined in config.yaml.
    '''
    parsed_resources = collections.defaultdict(lambda: collections.defaultdict(int))
    if config['resources']:
        for rule, specs in config['resources'].items():
            for spec, value in specs.items():
                parsed_resources[rule][spec] = value


def get_threads(rule):
    '''
    Get number of threads for a rule from the config dictionary.
    '''
    try:
        threads = config['resources'][rule]['threads']
    except KeyError:
        threads = config['resources']['default']['threads']
    return threads


def get_mem(rule, attempt):
    '''
    Get memory requirement for a rule from the config dictionary.
    Memory is increased 1.5x per attempt.
    '''
    try:
        mem_mb = config['resources'][rule]['mem_mb']
    except KeyError:
        mem_mb = config['resources']['default']['mem_mb']
    if isinstance(mem_mb, (int, float)):
        mem_mb = int(mem_mb * (1 + (attempt - 1) / 2))
    elif mem_mb.isdigit():
        mem_mb = int(int(mem_mb) * (1 + (attempt - 1) / 2))
    elif mem_mb[-1] in ('G', 'M', 'K'):  # Careful, this cannot be used in resources (not an int)
        tmp = float(mem_mb[:-1]) * (1 + (attempt - 1) / 2)
        mem_mb = f'{tmp}{mem_mb[-1]}'
    return mem_mb


def get_runtime(rule, attempt):
    '''
    Get runtime requirement for a rule from the config dictionary.
    Runtime is increased 1.5x per attempt.
    '''
    try:
        runtime = config['resources'][rule]['runtime_s']
    except KeyError:
        runtime = config['resources']['default']['runtime_s']
    if isinstance(runtime, int):
        time = runtime
    else:
        try:
            d, h, m, s = (int(f) for f in re.split(':|-', runtime))
            time = ((((d * 24) + h) * 60) + m) * 60 + s
        except ValueError:
            print(f'Invalid runtime format for rule <{rule}>: <{runtime}>')
            time = 3600
    # time = int(time * (1 + (attempt - 1) / 2))  # Remove this at the moment since curnagl has such a low max time limit
    return time


# Parse all resources specification sources and populate the config dictionary with resources for each rule
allocate_resources()
