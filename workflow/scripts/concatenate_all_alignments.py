import logging
from collections import defaultdict
from utils import setup_logging

# Define character to use when adding sequences to missing species
MISSING_SEQUENCE_CHAR = '-'

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)
    logging.info('Retrieving information from Snakemake')

    alignment_file_paths = snakemake.input
    concatenated_alignments_file_path = snakemake.output[0]
    taxids = snakemake.params.taxids

    concatenated_alignments = defaultdict(str)  # Dictionary to store the concatenated alignments
    for alignment_file_path in alignment_file_paths:
        orthogroup_taxid_list = set()
        with open(alignment_file_path) as alignment_file:
            for line in alignment_file:
                if line.startswith('>'):
                    seq_length = 0
                    taxid = line[1:].rstrip()
                    orthogroup_taxid_list.add(taxid)
                else:
                    sequence = line.rstrip()
                    concatenated_alignments[taxid] += sequence
                    seq_length += len(sequence)
        # For species missing from the alignment, a sequence of "-" is added (length = length of last sequence in alignment, doesn't really matter)
        for taxid in taxids:
            if taxid not in orthogroup_taxid_list:
                logging.info(f'Taxid {taxid} missing from the alignment file {alignment_file_path}, adding gap sequence based on last sequence length.')
                concatenated_alignments[taxid] += MISSING_SEQUENCE_CHAR * seq_length

    with open(concatenated_alignments_file_path, 'w') as concatenated_alignments_file:
        for taxid, sequence in concatenated_alignments.items():
            concatenated_alignments_file.write(f'>{taxid}\n{sequence}\n')
