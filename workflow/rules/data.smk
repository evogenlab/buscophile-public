# Constrain BUSCO ids to not start with a '.' to filter out hidden files
wildcard_constraints:
    busco_id = '[^.].+'

rule get_busco_ids_for_a_species:
    '''
    Extract the ID of all BUSCOs found for a species from BUSCO results.
    First, the best assembly for this species is selected (highest Complete BUSCOs),
    and its accession is saved in a txt file, then BUSCO IDs are extracted from the
    corresponding results tarball.
    '''
    output:
        buscos = 'results/busco_ids/{taxid}.txt',
        assembly = 'results/assemblies/{taxid}.txt'
    log:
        'logs/busco_ids/{taxid}.txt'
    conda:
        '../envs/data.yaml'
    params:
        busco_root_dir = config['busco']['root_dir'],
        busco_dataset = config['busco']['dataset']
    threads:
        get_threads('get_busco_ids_for_a_species')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('get_busco_ids_for_a_species', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('get_busco_ids_for_a_species', attempt)
    script:
        '../scripts/get_busco_ids_for_a_species.py'


checkpoint get_common_busco_ids:
    '''
    Compute the list of BUSCOs present at a frequency higher than or equal to min_frequency (see config file)
    in selected species. Output is a single txt file named after the BUSCO ID for each retained BUSCO.
    '''
    input:
        expand(rules.get_busco_ids_for_a_species.output.buscos, taxid=config['taxids'])
    output:
        directory('results/common_busco_ids')
    log:
        'logs/get_common_busco_ids.txt'
    conda:
        '../envs/data.yaml'
    params:
        min_frequency = config['busco']['min_frequency']
    threads:
        get_threads('get_common_busco_ids')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('get_common_busco_ids', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('get_common_busco_ids', attempt)
    script:
        '../scripts/get_common_busco_ids.py'


def create_busco_fasta_input(wildcards):
    '''
    Input function for create_busco_fasta. Triggers workflow reevaluation from the
    get_common_busco_ids checkpoint and returns the list of assembly accession numbers
    files that is used as input for this rule.
    '''
    checkpoints.get_common_busco_ids.get(**wildcards)
    assemblies = {taxid: rules.get_busco_ids_for_a_species.output.assembly.format(taxid=taxid) for
                  taxid in config['taxids']}
    return assemblies


rule create_busco_fasta:
    '''
    Generates a Fasta file containing the protein sequences of a BUSCO for all specified species.
    Sequences are extracted from the corresponding BUSCO results tarball and aggregated in a
    Fasta file with headers formatted as '>{taxid} {BUSCO id}'. If a BUSCO is absent in an assembly,
    it is excluded from the Fasta file (missing species are handled later when concatenating alignments).
    '''
    input:
        unpack(create_busco_fasta_input),
        common_buscos = rules.get_common_busco_ids.output
    output:
        'results/busco_sequences/{busco_id}.faa'
    log:
        'logs/create_busco_fasta/{busco_id}.txt'
    conda:
        '../envs/data.yaml'
    params:
        busco_root_dir = config['busco']['root_dir'],
        busco_dataset = config['busco']['dataset']
    threads:
        get_threads('create_busco_fasta')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('create_busco_fasta', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('create_busco_fasta', attempt)
    script:
        '../scripts/create_busco_fasta.py'


rule get_species_names:
    '''
    Generates a file of correspondence between taxid (user input) and species names from the A3Cat
    summary results.
    '''
    input:
        a3cat = os.path.join(config['busco']['root_dir'], 'results', 'summary.json'),
        taxid_list = config['species_file']
    output:
        'results/species_names.tsv'
    log:
        'logs/get_species_names.txt'
    conda:
        '../envs/data.yaml'
    threads:
        get_threads('get_species_names')
    resources:
        runtime_s = lambda wildcards, attempt: get_runtime('get_species_names', attempt),
        mem_mb = lambda wildcards, attempt: get_mem('get_species_names', attempt)
    script:
        '../scripts/get_species_names.py'
