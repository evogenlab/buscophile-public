import logging
import os
from collections import defaultdict
from utils import setup_logging, create_dir, touch

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    busco_ids_file_paths = snakemake.input
    output_dir_path = snakemake.output[0]
    min_frequency = snakemake.params.min_frequency

    # Get number of taxids to compute frequencies later
    n_taxids = len(busco_ids_file_paths)

    create_dir(output_dir_path)

    # Counts of presence in selected species for each BUSCO are saved in a dictionary
    # {busco_id: count}
    busco_counts = defaultdict(int)
    for busco_ids_file_path in busco_ids_file_paths:
        logging.info(f'Loading BUSCO ids from <{busco_ids_file_path}>')
        with open(busco_ids_file_path) as busco_ids_file:
            for line in busco_ids_file:
                if len(line) < 2:
                    continue
                busco_id = line.rstrip()
                busco_counts[busco_id] += 1

    n_common_buscos = 0
    for busco_id, count in busco_counts.items():
        frequency = count / n_taxids  # Frequency of BUSCO in selected species
        if frequency >= min_frequency:
            touch(os.path.join(output_dir_path, f'{busco_id}.txt'), 'w')
            n_common_buscos += 1

    logging.info(f'Successfully created output files for <{n_common_buscos}> common busco ids (min frequency: <{min_frequency}>)')
