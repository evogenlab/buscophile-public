import ete3
import logging
from utils import setup_logging

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)
    logging.info('Retrieving information from Snakemake')

    tree_file_path = snakemake.input.tree
    species_names_file_path = snakemake.input.species_names[0]
    tree_with_names_file_path = snakemake.output[0]

    logging.info('Reading species names file')
    species_names = {}
    with open(species_names_file_path) as species_names_file:
        for line in species_names_file:
            if len(line) < 2:
                continue
            taxid, species_name = line.rstrip().split('\t')
            species_names[taxid] = species_name

    logging.info(f'Reading input tree <{tree_file_path}>')
    # Read the tree with ete3 Tree class
    tree = ete3.Tree(tree_file_path)
    logging.info('Converting input tree')
    for node in tree.traverse('postorder'):
        if node.name in species_names:
            node.name = species_names[node.name]

    # Export the tree in a format required by MultiZ
    tree_with_names = tree.write(format=0)
    logging.info(f'Exporting output tree to  to <{tree_with_names_file_path}>')
    with open(tree_with_names_file_path, 'w') as output_file:
        output_file.write(tree_with_names)
    logging.info('Successfully converted tree')
